var request = require("supertest");
var server = require("./app.js");
var assert = require("assert");


it("statusCode should return 200", function(done)
{
    request(server)
        .get("/")
        .expect(200)
        .expect(function(res)
        {
            assert.equal(res.statusCode, 200);
        })
        .end(done);
});
